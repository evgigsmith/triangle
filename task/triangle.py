def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a + b > c and a + c > b and b + c > a and all([el > 0 for el in [a,b,c]]):
        return True
    return False